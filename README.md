# SSH Chat

A Peer to Peer chat client using [Hyper-Protocol](https://docs.holepunch.to/)


A high-level API for finding and connecting to peers who are interested in a "topic." 

[@hyperswarm/dht](https://docs.holepunch.to/building-blocks/hyperswarm#dht): 

A DHT powering Hyperswarm. Through this DHT, each server is bound to a unique key pair, with the client connecting to the server using the server's public key.


[@hyperswarm/secretstream](https://docs.holepunch.to/building-blocks/hyperswarm#secretstream): 

Secretstream is used to securely create connections between two peers in Hyperswarm.

## What is HyperSwarm?
Hyperswarm allows you to find and connect to peers who are announcing a common "topic". The swarm topic can be anything. This means that, with Hyperswarm, you can discover and connect peers with a shared interest over a distributed network. As an example, we often use Hypercore's discovery key as the swarm topic for discovering peers to replicate with.
Once Hyperswarm has discovered peers, nodes in the DHT help those peers connect via a UDP-based hole-punching algorithm that's designed to work well in home networks.

https://docs.holepunch.to/building-blocks/hyperswarm

## Usage

To install:

`git clone git@git.codingvm.codes:snxraven/sshChat-CLI.git`

`cd sshChat-CLI`

`npm i`

To run a new topic (room):

`node sshchat.js`

This will give you a connection topic to share:

```
/sshChat ❯ ✦ node sshChat.js
joined topic: 6076c0903ad293e24c10fceb501fe7b02425f6d26c7a5b2d015abd07e3e6b17b
(Share this key to others so they may join.)
You are now in a chatroom for your topic, feel free to chat.
```


To connect to an altready made topic (room) pass the hash on start up:


`node sshchat.mjs 6076c0903ad293e24c10fceb501fe7b02425f6d26c7a5b2d015abd07e3e6b17b`

# Commands

\>login [API KEY] - Login to the API 

\> command here - Send a command to your container

\>cd /to/path - Change your working directory.

>start - Start your container

>stop - Stop your container

>restart - restart your container

>stats - Get the containers stats

! [prompt] - Send a prompt to GPT3

## Screenshots:

A simple convo:
![ScreenShot](https://media.discordapp.net/attachments/1025463055016001546/1069366373576360077/image.png?width=1202&height=660)



If a user does not login they will have a random annon1234 username.

var unirest = require('unirest');

async function login(key, conns, MYKEY, USERNAME, DISCORD_USERID) {
    getUSERNAME(key)
    .then((data) => {
        console.log("Hello, " + data + "\nYou are now logged in.\n\n\n")
        USERNAME[0] = data
        MYKEY.push(key)
        LOGGEDIN = true
        getDISCORDID(key)
        .then((discord) => {
        DISCORD_USERID[0] = [discord]
        for (const conn of conns) {
            conn.write(`${USERNAME} | <@${discord}> is now logged in.`)
        }
    })
    })
    .catch(err => {
        console.log("Invalid Key")
        for (const conn of conns) {
            conn.write("Invalid Key, please try again.")
        }
    })
}


// API Functions
async function getUSERNAME(key) {
    let requestUSERNAME = await unirest
      .get('https://api.discord-linux.com/name')
      .headers({ 'Accept': 'application/json', 'Content-Type': 'application/json', 'x-discord-linux-auth': key })
    return requestUSERNAME.body.message
  }

  async function getDISCORDID(key) {
    let requestUSERNAME = await unirest
      .get('https://api.discord-linux.com/discordid')
      .headers({ 'Accept': 'application/json', 'Content-Type': 'application/json', 'x-discord-linux-auth': key })
    return requestUSERNAME.body.message
  }

module.exports = { login }
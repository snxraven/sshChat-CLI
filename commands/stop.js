// start.js
const unirest = require("unirest");

async function stopContainer(key) {
  let stopContainer = await unirest
      .get('https://api.discord-linux.com/stop')
      .headers({
          'Accept': 'application/json', 'Content-Type': 'application/json',
          'x-discord-linux-auth': key
      })
  return stopContainer.body
}

async function stop(key) {
  const response = await stopContainer(key)
  console.log(response)
}

module.exports = { stop, stopContainer }

// restart.js
const unirest = require("unirest");

async function restart(key) {

  async function restartContainer(key) {
    let restartContainer = await unirest
      .get('https://api.discord-linux.com/restart')
      .headers({
        'Accept': 'application/json', 'Content-Type': 'application/json',
        'x-discord-linux-auth': key
      })
    return restartContainer.body
  }

  const response = await restartContainer(key)
  console.log(response)
}

module.exports = { restart }
